    //Dom加载完毕事件监听器
    document.addEventListener('DOMContentLoaded', function () {
        // select all the elements with product__media media
        const mediaContainers = document.querySelectorAll('.product__media-item.slider__slide');
      
        //遍历每个容器
        mediaContainers.forEach(container => {
          //选择容器里的图像元素
          const image = container.querySelector('img');
          if (image) {
            //创建放大镜相关的DOM元素
            const smallDiv = document.createElement('div');
            const bigDiv = document.createElement('div');
            const bigImg = document.createElement('img');
            // the css of these zones
            smallDiv.style.cssText = `
              width: 100px;
              height: 100px;
              background-color: rgba(255, 0, 0, .2);
              display: none;
              position: absolute;
              pointer-events: none;
            `;
            bigDiv.style.cssText = `
              width: 400px;
              height: 400px;
              position: absolute;
              left: 900px;
              top: 400px;
              overflow: hidden;
              display: none;
              z-index: 9999999;
            `;
            bigImg.style.cssText = `
              position: absolute;
            `;
      //设置bigImg的源图像
            bigImg.src = image.src;
      //将创建的元素添加到DOM
            bigDiv.appendChild(bigImg);
            container.appendChild(smallDiv);
            container.appendChild(bigDiv);
      
            // on mouse over and on mouse out 当鼠标悬停的时候显示smalldiv和bigdiv；移出图像的时候隐藏
            image.onmouseover = function () {
              smallDiv.style.display = 'block';
              bigDiv.style.display = 'block';
            };
      
            image.onmouseout = function () {
              smallDiv.style.display = 'none';
              bigDiv.style.display = 'none';
            };
            //当鼠标在image移动的时候，函数会被调用
            image.onmousemove = function (e) {
              //rect是DOMRect对象，包含元素大小以及相对视口的位置
              const rect = container.getBoundingClientRect();
              const smallDivWidth = smallDiv.offsetWidth;
              const smallDivHeight = smallDiv.offsetHeight; // Maintain a square smallDiv
      
              //计算smallDiv的位置：x，y是鼠标位置减去容器左上角的位置，再减去smallDiv的一半宽和高，确保smallDiv中心与鼠标对齐
              let x = e.clientX - rect.left - smallDivWidth / 2;
              let y = e.clientY - rect.top - smallDivHeight / 2;
      
      
              //x不大于容器宽度减去smallDiv的宽度
      
      
              //y不小于0，这句不写的话，小div可以超出上框
              if (y <= 0) 
                y = 0;
              //x不小于0，如果这句代码不写，小div可以超出右框
              if(x <= 0)
                x = 0;
              //x不大于容器宽度减去smalldiv的宽度
              if(x >= image.naturalWidth - smallDivWidth)//这里要改成image.naturalwidth，小div才不会超出右边的范围
                x = image.naturalWidth - smallDivHeight;
              //y不大于容器高度减去smallDiv的高度
              if (y >= rect.height - smallDivHeight) 
                y = rect.height - smallDivHeight;
              
              //检测鼠标是否出容器的左边界
              if(x<smallDivWidth)
                x = smallDivWidth;
              
              // 这是小div的区域，.left和.top是分别距离左边框和上边框多少个单位
              smallDiv.style.left = x + 'px';
              smallDiv.style.top = y + 'px';
      
              // Move the bigImg
              const scale = 3;
      
              //假如下面的image换成rect，就会发现图片比container小好多，从我上面输出长宽也可以看出来
              bigImg.style.width = image.naturalWidth * scale + 'px';
              bigImg.style.height = image.naturalHeight * scale + 'px';
      
              //按理说这里表示鼠标相对于容器左上角的比例
              const offsetX = x / rect.width * image.naturalWidth * scale;
              const offsetY = y / rect.height * image.naturalHeight * scale;
              
              bigImg.style.left = -offsetX + 'px';
              bigImg.style.top = -offsetY + 'px';
      
            };
      
          }
      
        })
      
      });
      
      
      
      
      